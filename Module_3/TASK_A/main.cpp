#include <cmath>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

const double eps = 1e-9;
struct Point {
  Point(double x, double y, double z) : x_(x), y_(y), z_(z) {}
  double x_;
  double y_;
  double z_;
  bool operator==(const Point &other) const {
    return (std::abs(x_ - other.x_) < eps) && (std::abs(y_ - other.y_) < eps) &&
           (std::abs(z_ - other.z_) < eps);
  }
  Point operator-() { return Point(-x_, -y_, -z_); }
  Point &operator=(const Point &other) = default;
};

double Distance(const Point &first, const Point &other) {
  return std::sqrt((first.x_ - other.x_) * (first.x_ - other.x_) +
                   (first.y_ - other.y_) * (first.y_ - other.y_) +
                   (first.z_ - other.z_) * (first.z_ - other.z_));
}

struct RadiusVector {
  explicit RadiusVector(const Point &begin, const Point &end)
      : end_(end.x_ - begin.x_, end.y_ - begin.y_, end.z_ - begin.z_) {}
  explicit RadiusVector(double x, double y, double z) : end_(Point(x, y, z)) {}
  explicit RadiusVector(const Point &point) : end_(point) {}
  double Size() const {
    return std::sqrt(end_.x_ * end_.x_ + end_.y_ * end_.y_ + end_.z_ * end_.z_);
  }
  double operator*(const RadiusVector &other) const {
    return (end_.x_ * other.end_.x_ + end_.y_ * other.end_.y_ +
            end_.z_ * other.end_.z_);
  }
  RadiusVector operator%(const RadiusVector &other) {
    return RadiusVector(
        end_.y_ * other.end_.z_ - end_.z_ * other.end_.y_,
        -1 * (end_.x_ * other.end_.z_ - end_.z_ * other.end_.x_),
        end_.x_ * other.end_.y_ - end_.y_ * other.end_.x_);
  }
  RadiusVector operator-(const RadiusVector &other) {
    return RadiusVector(end_.x_ - other.end_.x_, end_.y_ - other.end_.y_,
                        end_.z_ - other.end_.z_);
  }
  RadiusVector operator+(const RadiusVector &other) {
    return RadiusVector(end_.x_ + other.end_.x_, end_.y_ + other.end_.y_,
                        end_.z_ + other.end_.z_);
  }
  bool operator==(const RadiusVector &other) const {
    return end_ == other.end_;
  }
  RadiusVector operator-() const {  // унарный минус
    return RadiusVector(-end_.x_, -end_.y_, -end_.z_);
  }
  RadiusVector operator*(const double &k) {
    return RadiusVector(end_.x_ * k, end_.y_ * k, end_.z_ * k);
  }
  RadiusVector Abs() {
    return RadiusVector(std::abs(end_.x_), std::abs(end_.y_),
                        std::abs(end_.z_));
  }
  double Size() {
    return std::sqrt(end_.x_ * end_.x_ + end_.y_ * end_.y_ + end_.z_ * end_.z_);
  }
  Point end_;
};

struct Segment {
  Segment(const Point &point, const RadiusVector &radius_vector)
      : radius_vector_(radius_vector), begin_(point) {}

  bool IsDegenerate() const { return radius_vector_ == RadiusVector(0, 0, 0); }
  double Size() const { return radius_vector_.Size(); }
  RadiusVector radius_vector_;
  Point begin_;
};

Point Move(const Point &point, const RadiusVector &radius_vector) {
  return {point.x_ + radius_vector.end_.x_, point.y_ + radius_vector.end_.y_,
          point.z_ + radius_vector.end_.z_};
}

bool IsObtuseAngle(const RadiusVector &first, const RadiusVector &second) {
  return (first * second) < 0;
}

double DistancePointAndSegment(Point &point, const Segment &segment) {
  if (segment.IsDegenerate()) {
    return Distance(point, segment.begin_);
  }
  double distance_to_begin = Distance(point, segment.begin_);
  double distance_to_end =
      Distance(point, Move(segment.begin_, segment.radius_vector_));
  RadiusVector first_side = RadiusVector(segment.begin_, point);
  RadiusVector second_side =
      RadiusVector(Move(segment.begin_, segment.radius_vector_), point);
  double area = (first_side % second_side).Size() / 2;
  double distance_to_segment = 2 * area / segment.Size();
  if (IsObtuseAngle(first_side, segment.radius_vector_) ||
      IsObtuseAngle(second_side, -segment.radius_vector_)) {
    return std::min(distance_to_begin, distance_to_end);
  }
  return distance_to_segment;
}

double DistanceRadiusVectorAndSegment(RadiusVector &radius_vector,
                                      const Segment &segment) {
  return DistancePointAndSegment(radius_vector.end_, segment);
}

template <typename T, typename Function>
auto TernarySearch(T left, T right,
                   const Function &distance) {  // car qual
  RadiusVector absolute = (left - right).Abs();
  if (absolute.Size() < eps) {
    return distance(left);
  }

  T first_candidate = (right - left) * (static_cast<double>(1) / 3) + left;
  auto first_distance = distance(first_candidate);

  T second_candidate = (right - left) * (static_cast<double>(2) / 3) + left;

  auto second_distance = distance(second_candidate);
  if (first_distance < second_distance) {
    return TernarySearch(left, second_candidate, distance);
  }
  return TernarySearch(first_candidate, right, distance);
}

int main() {
  double x1 = 0;
  double y1 = 0;
  double z1 = 0;
  double x2 = 0;
  double y2 = 0;
  double z2 = 0;
  cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
  Point p1 = Point(x1, y1, z1);
  Point p2 = Point(x2, y2, z2);
  Segment first_segment = Segment(p1, RadiusVector(p1, p2));

  cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2;
  Point p3 = Point(x1, y1, z1);
  Point p4 = Point(x2, y2, z2);
  Segment second_segment = Segment(p3, RadiusVector(p3, p4));

  auto distance = [&](RadiusVector x) {
    return DistanceRadiusVectorAndSegment(x, second_segment);
  };

  const size_t precision = 6;
  std::cout.precision(precision);
  std::cout.setf(std::ios::fixed);
  cout << TernarySearch(
      RadiusVector(first_segment.begin_),
      RadiusVector(first_segment.begin_) + first_segment.radius_vector_,
      distance);
}
