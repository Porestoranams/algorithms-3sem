#include <cmath>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

const double eps = 1e-10;

struct RadiusVector {
  explicit RadiusVector(double x, double y) : x_(x), y_(y) {}
  RadiusVector(const RadiusVector &radius_vector) = default;
  double operator*(const RadiusVector &other) const {
    return (x_ * other.x_ + y_ * other.y_);
  }

  RadiusVector operator-(const RadiusVector &other) const {
    return RadiusVector(x_ - other.x_, y_ - other.y_);
  }
  bool operator==(const RadiusVector &other) const {
    return ((std::abs(x_ - other.x_) < eps) && (std::abs(y_ - other.y_) < eps));
  }
  RadiusVector operator*(const double k) const {
    return RadiusVector(x_ * k, y_ * k);
  }
  RadiusVector operator+(const RadiusVector &other) const {
    return RadiusVector(x_ + other.x_, y_ + other.y_);
  }
  bool operator%(const RadiusVector &other) const {
    return x_ * other.y_ - y_ * other.x_ > 0;
  }

  double x_;
  double y_;
};

bool IsUpdatedMaxPoint(double &x_max, double &y_max, const double x,
                       const double y) {
  if (y > y_max) {
    y_max = y;
    x_max = x;
    return true;
  }
  if (y == y_max) {
    if (x > x_max) {
      x_max = x;
    }
    return true;
  }
  return false;
}

bool Cmp(const RadiusVector &first, const RadiusVector &second) {
  return !(first % second);
}

bool CheckIntersection(const std::vector<RadiusVector> &points) {
  bool orientation = (points[points.size() - 1] % points[0]);
  for (size_t i = 0; i < points.size() - 1; ++i) {
    if ((points[i] % points[i + 1]) != orientation) {
      return false;
    }
  }
  return true;
}

vector<RadiusVector> CalculateMinkowskiSum(
    const vector<RadiusVector> &vectors_first_figure,
    const vector<RadiusVector> &vectors_second_figure, RadiusVector first,
    const size_t pos_start_point_in_1_figure,
    const size_t pos_start_point_in_2_figure) {
  vector<RadiusVector> minkowski_sum;

  size_t num_points_1 = vectors_first_figure.size();
  size_t num_points_2 = vectors_second_figure.size();

  minkowski_sum.push_back(first);

  size_t cur_pos_in_1 = pos_start_point_in_1_figure;
  size_t cur_pos_in_2 = pos_start_point_in_2_figure;

  for (size_t k = 0; k < num_points_1 + num_points_2; ++k) {
    RadiusVector prev_point = minkowski_sum.back();
    if (Cmp(vectors_first_figure[cur_pos_in_1 % num_points_1],
            vectors_second_figure[cur_pos_in_2 % num_points_2])) {
      minkowski_sum.push_back(
          prev_point + vectors_first_figure[cur_pos_in_1 % num_points_1]);
      ++cur_pos_in_1;
    } else {
      minkowski_sum.push_back(
          prev_point + vectors_second_figure[cur_pos_in_2 % num_points_2]);
      ++cur_pos_in_2;
    }
  }

  minkowski_sum.pop_back();
  return minkowski_sum;
}

void Initialization(vector<RadiusVector> &vector_edges_in_figure,
                    RadiusVector &first_point, const size_t num_points,
                    int coef) {
  double prev_x = 0;
  double prev_y = 0;

  cin >> prev_x;
  cin >> prev_y;

  prev_x *= coef;
  prev_y *= coef;

  first_point = RadiusVector(prev_x, prev_y);

  for (size_t i = 1; i < num_points; ++i) {
    double x = 0;
    double y = 0;
    cin >> x >> y;

    x *= coef;
    y *= coef;

    vector_edges_in_figure.emplace_back(x - prev_x, y - prev_y);
    prev_x = x;
    prev_y = y;
  }
  vector_edges_in_figure.emplace_back(first_point -
                                      RadiusVector(prev_x, prev_y));
}

size_t FindMaxPoint(const vector<RadiusVector> &vector_edges_in_figure,
                    RadiusVector first_point, RadiusVector &start_point) {
  double x_max = -INFINITY;
  double y_max = -INFINITY;

  IsUpdatedMaxPoint(x_max, y_max, first_point.x_, first_point.y_);

  RadiusVector prev_point = first_point;
  const size_t num_points = vector_edges_in_figure.size();
  size_t position_of_max_point = 0;
  for (size_t i = 0; i < num_points - 1; ++i) {
    RadiusVector cur_vector = prev_point + vector_edges_in_figure[i];
    if (IsUpdatedMaxPoint(x_max, y_max, cur_vector.x_, cur_vector.y_)) {
      position_of_max_point = i + 1;
    }
    prev_point = cur_vector;
  }
  position_of_max_point %= num_points;
  start_point = RadiusVector(x_max, y_max);
  return position_of_max_point;
}

int main() {
  size_t num_points_1 = 0;
  cin >> num_points_1;
  vector<RadiusVector> vectors_first_figure;
  RadiusVector first_point_in_first_figure = RadiusVector(0, 0);

  Initialization(vectors_first_figure, first_point_in_first_figure,
                 num_points_1, 1);
  RadiusVector start_point_in_first_figure = RadiusVector(0, 0);
  size_t position_of_start_in_first_figure =
      FindMaxPoint(vectors_first_figure, first_point_in_first_figure,
                   start_point_in_first_figure);

  size_t num_points_2 = 0;
  cin >> num_points_2;
  vector<RadiusVector> vectors_second_figure;
  RadiusVector first_point_in_second_figure = RadiusVector(0, 0);

  Initialization(vectors_second_figure, first_point_in_second_figure,
                 num_points_2, -1);
  RadiusVector start_point_in_second_figure = RadiusVector(0, 0);
  size_t position_of_start_in_second_figure =
      FindMaxPoint(vectors_second_figure, first_point_in_second_figure,
                   start_point_in_second_figure);

  RadiusVector first_point_in_minkowski = RadiusVector(
      start_point_in_first_figure.x_ + start_point_in_second_figure.x_,
      start_point_in_first_figure.y_ + start_point_in_second_figure.y_);

  vector<RadiusVector> minkowski_sum = CalculateMinkowskiSum(
      vectors_first_figure, vectors_second_figure, first_point_in_minkowski,
      position_of_start_in_first_figure, position_of_start_in_second_figure);
  if (CheckIntersection(minkowski_sum)) {
    cout << "YES";
  } else {
    cout << "NO";
  }
  return 0;
}