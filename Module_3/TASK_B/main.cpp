#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <queue>
#include <vector>

using std::cin;
using std::cout;
using std::queue;
using std::string;
using std::vector;

const double eps = 1e-9;

struct RadiusVector {
  explicit RadiusVector(double x, double y, double z) : x_(x), y_(y), z_(z) {}
  RadiusVector(const RadiusVector &radius_vector) = default;
  double Size() const { return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_); }
  double operator*(const RadiusVector &other) const {
    return (x_ * other.x_ + y_ * other.y_ + z_ * other.z_);
  }
  RadiusVector operator%(const RadiusVector &other) const {
    return RadiusVector(y_ * other.z_ - z_ * other.y_,
                        -1 * (x_ * other.z_ - z_ * other.x_),
                        x_ * other.y_ - y_ * other.x_);
  }
  RadiusVector operator-(const RadiusVector &other) const {
    return RadiusVector(x_ - other.x_, y_ - other.y_, z_ - other.z_);
  }
  bool operator==(const RadiusVector &other) const {
    return ((std::abs(x_ - other.x_) < eps) &&
            (std::abs(y_ - other.y_) < eps) && (std::abs(z_ - other.z_) < eps));
  }
  RadiusVector operator-() const {  // унарный минус
    return RadiusVector(-x_, -y_, -z_);
  }
  RadiusVector operator*(const double &k) const {
    return RadiusVector(x_ * k, y_ * k, z_ * k);
  }
  RadiusVector operator+(const RadiusVector &other) const {
    return RadiusVector(x_ + other.x_, y_ + other.y_, z_ + other.z_);
  }

  double x_;
  double y_;
  double z_;
};

struct Face {
  explicit Face(const size_t x, const size_t y, const size_t z)
      : a_(x), b_(y), c_(z) {}
  size_t a_;
  size_t b_;
  size_t c_;
  bool operator<(const Face &other) {
    if (a_ == other.a_) {
      if (b_ == other.b_) {
        if (c_ == other.c_) {
          return false;
        }
        return c_ < other.c_;
      }
      return b_ < other.b_;
    }
    return a_ < other.a_;
  }
};

bool MixedProductOrientation(const RadiusVector &first_radius_vector,
                             const RadiusVector &second_radius_vector,
                             const RadiusVector &third_radius_vector) {
  return (first_radius_vector.x_ * second_radius_vector.y_ *
              third_radius_vector.z_ +
          second_radius_vector.x_ * first_radius_vector.z_ *
              third_radius_vector.y_ +
          third_radius_vector.x_ * first_radius_vector.y_ *
              second_radius_vector.z_ -
          first_radius_vector.z_ * second_radius_vector.y_ *
              third_radius_vector.x_ -
          first_radius_vector.x_ * second_radius_vector.z_ *
              third_radius_vector.y_ -
          first_radius_vector.y_ * second_radius_vector.x_ *
              third_radius_vector.z_) > 0;
}

double CosAngle(const RadiusVector &radius_vector) {
  if (radius_vector.y_ == 0 && radius_vector.x_ == 0) {
    return 1;
  }
  return ((radius_vector.y_ == 0)
              ? (0)
              : ((-radius_vector.y_) /
                 std::sqrt(radius_vector.x_ * radius_vector.x_ +
                           radius_vector.y_ * radius_vector.y_)));
}

size_t SearchSecondPoint(const vector<RadiusVector> &radiuses,
                         const size_t first_radius_vector) {
  double cos_alpha = -2;
  size_t second_radius_vector = 0;
  for (size_t i = 0; i < radiuses.size(); ++i) {
    if (first_radius_vector == i) {
      continue;
    }
    RadiusVector cur_radius_vector =
        radiuses[i] - radiuses[first_radius_vector];
    double cur_cos = CosAngle(cur_radius_vector);
    if (cur_cos > cos_alpha) {
      second_radius_vector = i;
      cos_alpha = cur_cos;
    }
  }
  return second_radius_vector;
}

size_t SearchFirstCandidateFace(const size_t first_point,
                                const size_t second_point) {
  if (first_point != 0 && second_point != 0) {
    return 0;
  }
  if (first_point != 1 && second_point != 1) {
    return 1;
  }
  return 2;
}

size_t SearchAnyPointDifferentFromCurrents(const size_t first_point,
                                           const size_t second_point,
                                           const size_t third_point) {
  const size_t face_size = 3;
  for (size_t i = 0; i < face_size + 1; ++i) {
    if (i == first_point || i == second_point || i == third_point) {
      continue;
    }
    return i;
  }
  return 0;
}

void UpdatePointsOccurences(std::map<std::pair<size_t, size_t>, size_t> &edges,
                            const Face &face) {
  ++edges[{std::min(face.a_, face.b_), std::max(face.a_, face.b_)}];
  ++edges[{std::min(face.a_, face.c_), std::max(face.a_, face.c_)}];
  ++edges[{std::min(face.c_, face.b_), std::max(face.c_, face.b_)}];
}

Face MakeCorrectFace(const vector<RadiusVector> &radiuses,
                     const size_t first_point, const size_t second_point,
                     const size_t third_point) {
  size_t any_point = SearchAnyPointDifferentFromCurrents(
      first_point, second_point, third_point);
  size_t min_point = std::min(first_point, std::min(second_point, third_point));
  if (min_point == first_point) {
    bool orientation =
        MixedProductOrientation(radiuses[second_point] - radiuses[first_point],
                                radiuses[third_point] - radiuses[first_point],
                                radiuses[any_point] - radiuses[first_point]);
    if (orientation) {
      return Face(first_point, third_point, second_point);
    }
    return Face(first_point, second_point, third_point);
  }
  if (min_point == second_point) {
    bool orientation =
        MixedProductOrientation(radiuses[first_point] - radiuses[second_point],
                                radiuses[third_point] - radiuses[second_point],
                                radiuses[any_point] - radiuses[second_point]);
    if (orientation) {
      return Face(second_point, third_point, first_point);
    }
    return Face(second_point, first_point, third_point);
  }
  bool orientation =
      MixedProductOrientation(radiuses[first_point] - radiuses[third_point],
                              radiuses[second_point] - radiuses[third_point],
                              radiuses[any_point] - radiuses[third_point]);
  if (orientation) {
    return Face(third_point, second_point, first_point);
  }
  return Face(third_point, first_point, second_point);
}

Face SearchNextFace(const vector<RadiusVector> &radiuses,
                    std::map<std::pair<size_t, size_t>, size_t> &edges,
                    const size_t first_point, const size_t second_point,
                    const size_t third_point) {
  size_t candidate = SearchAnyPointDifferentFromCurrents(
      first_point, second_point, third_point);
  RadiusVector radius_vector_1 = radiuses[second_point] - radiuses[first_point];
  RadiusVector radius_vector_2 = radiuses[candidate] - radiuses[first_point];
  bool cur_orientation =
      MixedProductOrientation(radius_vector_1, radius_vector_2,
                              radiuses[third_point] - radiuses[first_point]);
  for (size_t i = 0; i < radiuses.size(); ++i) {
    if (i != candidate && i != first_point && i != second_point &&
        i != third_point) {
      bool new_orientation =
          MixedProductOrientation(radius_vector_1, radius_vector_2,
                                  radiuses[i] - radiuses[first_point]);
      if (new_orientation != cur_orientation) {
        RadiusVector new_candidate_vector = radiuses[i] - radiuses[first_point];
        cur_orientation = MixedProductOrientation(
            radius_vector_1, new_candidate_vector, radius_vector_2);  // here
        radius_vector_2 = new_candidate_vector;
        candidate = i;
      }
    }
  }
  Face new_face =
      MakeCorrectFace(radiuses, first_point, second_point, candidate);
  UpdatePointsOccurences(edges, new_face);
  return new_face;
}

bool CanMakeNewFace(std::map<std::pair<size_t, size_t>, size_t> &edges,
                    const size_t first_point, const size_t second_point) {
  std::pair<size_t, size_t> edge = {std::min(first_point, second_point),
                                    std::max(first_point, second_point)};
  if (edges.find(edge) != edges.end()) {
    return edges[edge] < 2;
  }
  edges[edge] = 0;
  return true;
}

void BuildConvexHull(vector<Face> &faces, const vector<RadiusVector> &radiuses,
                     std::map<std::pair<size_t, size_t>, size_t> &edges,
                     const Face &first_face) {
  queue<Face> queue_of_faces;
  queue_of_faces.push(first_face);
  while (!queue_of_faces.empty()) {
    faces.push_back(queue_of_faces.front());
    size_t first_point = queue_of_faces.front().a_;
    size_t second_point = queue_of_faces.front().b_;
    size_t third_point = queue_of_faces.front().c_;
    queue_of_faces.pop();
    if (CanMakeNewFace(edges, first_point, second_point)) {
      Face next_face = SearchNextFace(radiuses, edges, first_point,
                                      second_point, third_point);
      queue_of_faces.push(next_face);
    }
    if (CanMakeNewFace(edges, first_point, third_point)) {
      Face next_face = SearchNextFace(radiuses, edges, first_point, third_point,
                                      second_point);
      queue_of_faces.push(next_face);
    }
    if (CanMakeNewFace(edges, second_point, third_point)) {
      Face next_face = SearchNextFace(radiuses, edges, second_point,
                                      third_point, first_point);
      queue_of_faces.push(next_face);
    }
  }
}

Face SearchFirstFace(vector<RadiusVector> &radiuses, const size_t first_point) {
  size_t second_point = SearchSecondPoint(radiuses, first_point);
  RadiusVector cur_first_vector =
      radiuses[second_point] - radiuses[first_point];

  size_t third_point = SearchFirstCandidateFace(first_point, second_point);
  RadiusVector cur_second_vector =
      radiuses[third_point] - radiuses[first_point];

  size_t any_point = SearchAnyPointDifferentFromCurrents(
      first_point, second_point, third_point);

  bool cur_orientation =
      MixedProductOrientation(cur_first_vector, cur_second_vector,
                              radiuses[any_point] - radiuses[first_point]);

  for (size_t k = 0; k < radiuses.size(); ++k) {
    if (k == first_point || k == second_point || k == third_point) {
      continue;
    }
    RadiusVector cur_radius_vector = radiuses[k] - radiuses[first_point];
    bool new_orientation = MixedProductOrientation(
        cur_first_vector, cur_second_vector, cur_radius_vector);
    if (cur_orientation == new_orientation) {
      continue;
    }
    RadiusVector new_cur_vector_candidate = radiuses[k] - radiuses[first_point];

    cur_orientation = MixedProductOrientation(
        cur_first_vector, new_cur_vector_candidate, cur_second_vector);
    cur_second_vector = new_cur_vector_candidate;
    third_point = k;
  }
  return MakeCorrectFace(radiuses, first_point, second_point, third_point);
}

int main() {
  size_t m = 0;
  cin >> m;
  const double max_x = 1000;
  for (size_t i = 0; i < m; ++i) {
    size_t n = 0;
    cin >> n;
    double min_x = max_x;
    size_t first_point = 0;
    vector<RadiusVector> radiuses;
    for (size_t j = 0; j < n; ++j) {
      double x = 0;
      double y = 0;
      double z = 0;
      cin >> x >> y >> z;
      if (x < min_x) {
        first_point = radiuses.size();
        min_x = x;
      }
      radiuses.emplace_back(x, y, z);
    }

    std::map<std::pair<size_t, size_t>, size_t> edges;
    vector<Face> faces;

    Face first_face = SearchFirstFace(radiuses, first_point);
    UpdatePointsOccurences(edges, first_face);

    BuildConvexHull(faces, radiuses, edges, first_face);
    std::sort(faces.begin(), faces.end());
    size_t num_of_faces = faces.size();
    const size_t face_size = 3;
    cout << num_of_faces << '\n';
    for (const auto &face : faces) {
      cout << face_size << ' ' << face.a_ << ' ' << face.b_ << ' ' << face.c_;
      cout << '\n';
    }
  }
}
