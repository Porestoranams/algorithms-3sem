#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

const size_t one_digit_in_block = 10;

struct BigInteger {
 public:
  explicit BigInteger(long long number) {
    positive_ = true;
    if (number == 0) {
      return;
    }
    if (number < 0) {
      positive_ = false;
      number *= (-1);
    }
    nums_.push_back(static_cast<uint32_t>(number % base));
    if (number >= base) {
      nums_.push_back(static_cast<uint32_t>(number / base));
    }
  }

  BigInteger(int a) {
    if (a == 0) {
      positive_ = true;
    } else if (a < 0) {
      positive_ = false;
      nums_.push_back(static_cast<uint32_t>(-a));
    } else {
      positive_ = true;
      nums_.push_back(static_cast<uint32_t>(a));
    }
  }

  explicit BigInteger(const string &number_string) {
    int pos_of_first_symbol = 0;
    if (number_string[0] == '-') {
      positive_ = false;
      pos_of_first_symbol = 1;
    }
    uint32_t cur_number = 0;
    uint32_t cur_base_ten = 1;
    int max_pos_of_string_number = static_cast<int>(number_string.size() - 1);
    for (int i = max_pos_of_string_number; i >= pos_of_first_symbol; --i) {
      cur_number +=
          cur_base_ten * (static_cast<uint32_t>(
                             number_string[static_cast<uint32_t>(i)] - '0'));
      if (cur_base_ten != base / one_digit_in_block) {
        cur_base_ten *= one_digit_in_block;
      } else {
        cur_base_ten = 1;
        nums_.push_back(cur_number);
        cur_number = 0;
      }
    }
    if (cur_number != 0) {
      nums_.push_back(cur_number);
    }
    // if
    if (nums_.empty()) {
      positive_ = true;
    }
  }

  BigInteger() : positive_(true) {}

  explicit operator bool() const { return !(this->IsZero()); }

  explicit operator int() const {
    int number = 0;
    for (size_t i = 0; i < this->Size(); ++i) {
      number *= base;
      number += this->nums_[i];
    }
    if (!this->positive_) {
      number *= (-1);
    }
    return number;
  }

  size_t Size() const { return nums_.size(); }

  BigInteger Abs() const {
    BigInteger other = *this;
    if (!this->positive_) {
      other.positive_ = true;
    }
    return other;
  }

  bool IsZero() const { return nums_.empty(); }

  string toString() const {
    if (this->IsZero()) {
      return "0";
    }
    string cur_number_string;
    for (size_t i = 0; i < nums_.size(); ++i) {
      if (i == nums_.size() - 1) {
        cur_number_string += ConvertFirstBlockToString(nums_[i]);
        continue;
      }
      cur_number_string += ConvertUIToString(nums_[i]);
    }
    if (!positive_) {
      cur_number_string += '-';
    }
    for (size_t i = 0; i < cur_number_string.size() / 2; ++i) {
      std::swap(cur_number_string[i],
                cur_number_string[cur_number_string.size() - 1 - i]);
    }
    return cur_number_string;
  }

  BigInteger &operator=(const BigInteger &other) {
    if (this == &other) {
      return *this;
    }
    positive_ = other.positive_;
    nums_ = other.nums_;
    return *this;
  }

  // multiplication by (10 ^ 9) ^ count
  BigInteger &InsertZeros(const size_t &count) {
    nums_.insert(nums_.begin(), count, 0);
    return *this;
  }

  BigInteger &operator+=(const BigInteger &other) {
    if (this == &other) {
      BigInteger big_integer = *this;
      return *this += big_integer;
    }
    if (other.positive_ == this->positive_) {
      return this->AdditionPositiveBI(other);
    }
    if (this->positive_) {
      return this->SubtractionPositiveBI(other.Abs());
    }
    this->positive_ = true;
    this->SubtractionPositiveBI(other);
    if (!this->IsZero()) {
      this->positive_ = !this->positive_;
    }
    return *this;
  }

  BigInteger &operator-=(const BigInteger &other) {
    if (this == &other) {
      *this = BigInteger(0);
      return *this;
    }
    if (positive_ && other.positive_) {
      return this->SubtractionPositiveBI(other);
    }
    if (!positive_ && !other.positive_) {
      this->positive_ = true;
      this->SubtractionPositiveBI(other.Abs());
      this->positive_ = !this->positive_;
      return *this;
    }
    return this->AdditionPositiveBI(other);
  }

  BigInteger &operator--() { return (*this -= BigInteger(1)); }

  BigInteger &operator++() { return (*this += BigInteger(1)); }

  BigInteger operator++(int) {
    BigInteger big_integer = *this;
    ++(*this);
    return big_integer;
  }

  BigInteger operator-() const {
    if (this->IsZero()) {
      return BigInteger(0);
    }
    BigInteger new_big_int = *this;
    new_big_int.positive_ = !(this->positive_);
    return new_big_int;
  }

  friend bool operator<(const BigInteger &first, const BigInteger &second);
  friend bool operator>(const BigInteger &first, const BigInteger &second);
  friend bool operator<=(const BigInteger &first, const BigInteger &second);
  friend bool operator>=(const BigInteger &first, const BigInteger &second);
  friend bool operator==(const BigInteger &first, const BigInteger &second);
  friend bool operator!=(const BigInteger &first, const BigInteger &second);

  friend BigInteger operator*(const BigInteger &first,
                              const BigInteger &second);

  friend BigInteger Shift(const size_t &first_block, const size_t &second_block,
                          const BigInteger &big_integer);

  friend BigInteger operator/(const BigInteger &first,
                              const BigInteger &second);

  friend BigInteger operator+(const BigInteger &first,
                              const BigInteger &second);

  friend BigInteger operator%(const BigInteger &dividend,
                              const BigInteger &divider);

  friend BigInteger operator-(const BigInteger &first,
                              const BigInteger &second);

  BigInteger &operator%=(const BigInteger &big_integer) {
    if (this == &big_integer) {
      *this = BigInteger(0);
      return *this;
    }
    *this = *this % big_integer;
    return *this;
  }

  BigInteger &operator*=(const BigInteger &big_integer) {
    *this = *this * big_integer;
    return *this;
  }

  BigInteger &operator/=(const BigInteger &big_integer) {
    if (this == &big_integer) {
      *this = BigInteger(1);
      return *this;
    }
    *this = *this / big_integer;
    return *this;
  }

 private:
  // for positive numbers
  BigInteger &AdditionPositiveBI(const BigInteger &other) {
    auto first_size = static_cast<uint32_t>(this->nums_.size());
    auto second_size = static_cast<uint32_t>(other.nums_.size());
    bool overflow = false;
    for (size_t i = 0; i < std::min(first_size, second_size); ++i) {
      uint32_t sum_of_blocks = this->nums_[i] + other.nums_[i];
      if (overflow) {
        ++sum_of_blocks;
      }
      this->nums_[i] = sum_of_blocks % (base);
      overflow = (sum_of_blocks >= base);
    }
    if (first_size == second_size && overflow) {
      nums_.push_back(1);
    }
    if (first_size > second_size && overflow) {
      for (size_t i = second_size; i < first_size; ++i) {
        if (nums_[i] == base - 1) {
          nums_[i] = 0;
        } else {
          nums_[i] += 1;
          overflow = false;
          break;
        }
      }
      if (overflow) {
        nums_.push_back(1);
      }
    } else if (first_size < second_size) {
      if (!overflow) {
        for (size_t i = first_size; i < second_size; ++i) {
          nums_.push_back(other.nums_[i]);
        }
      } else {
        CaseWithoutOverflowAndFirstSizeGreater(first_size, second_size, other);
      }
    }
    return *this;
  }

  void CaseWithoutOverflowAndFirstSizeGreater(size_t first_size,
                                              size_t second_size,
                                              const BigInteger &other) {
    bool overflow = true;
    for (size_t i = first_size; i < second_size; ++i) {
      if (overflow && (other.nums_[i] == base - 1)) {
        nums_.push_back(0);
      } else {
        if (overflow) {
          nums_.push_back(other.nums_[i] + 1);
          overflow = false;
        } else {
          nums_.push_back(other.nums_[i]);
        }
      }
    }
    if (overflow) {
      nums_.push_back(1);
    }
  }

  BigInteger &SubtractionPositiveBI(const BigInteger &other) {
    if (*this >= other) {
      int num_of_max_block = static_cast<int>(other.Size() - 1);
      for (int i = num_of_max_block; i >= 0; --i) {
        auto cur_position = static_cast<uint32_t>(i);
        if (this->nums_[cur_position] >= other.nums_[cur_position]) {
          this->nums_[cur_position] -= other.nums_[cur_position];
          continue;
        }
        uint64_t res_of_substraction = this->nums_[cur_position] + base;
        this->nums_[cur_position] = static_cast<uint32_t>(
            res_of_substraction - other.nums_[cur_position]);
        uint32_t pos_for_borrow = cur_position + 1;
        while (this->nums_[pos_for_borrow] == 0) {
          this->nums_[pos_for_borrow] = base - 1;
          ++pos_for_borrow;
        }
        --this->nums_[pos_for_borrow];
      }
      this->Trimmer();
      return *this;
    }
    BigInteger new_other = other;
    new_other.SubtractionPositiveBI(*this);
    *this = new_other;
    this->positive_ = false;
    return *this;
  }

  // delete leading zeros
  void Trimmer() {
    if (this->Size() == 0) {
      return;
    }
    while (this->Size() != 0 && this->nums_.back() == 0) {
      nums_.pop_back();
    }
    if (this->Size() == 0) {
      positive_ = true;
    }
  }

  string ConvertFirstBlockToString(uint32_t number) const {
    string number_string;
    while (number != 0) {
      number_string += char(number % one_digit_in_block + '0');
      number /= one_digit_in_block;
    }
    return number_string;
  }

  string ConvertUIToString(uint32_t number) const {
    string number_string;
    for (size_t i = 0; i < num_of_digits_in_block; ++i) {
      number_string += char(number % one_digit_in_block + '0');
      number /= one_digit_in_block;
    }
    return number_string;
  }

  vector<uint32_t> nums_;
  bool positive_ = true;
  const uint32_t base = 1000000000;
  const uint32_t num_of_digits_in_block = 9;
};

// returns the part of BigInteger according to passing numbers of integers
// blocks
BigInteger Shift(const size_t &first_block, const size_t &second_block,
                 const BigInteger &big_integer) {
  BigInteger new_big_integer;
  for (size_t i = first_block; i < second_block; ++i) {
    new_big_integer.nums_.push_back(big_integer.nums_[i]);
  }
  new_big_integer.Trimmer();
  return new_big_integer;
}

// multiplication by (10 ^ 9) ^ count
BigInteger InsertionOfZero(const BigInteger &big_integer, const size_t count) {
  BigInteger new_big_integer = big_integer;
  return new_big_integer.InsertZeros(count);
}

BigInteger operator+(const BigInteger &first, const BigInteger &second) {
  BigInteger big_integer = first;
  big_integer += second;
  return big_integer;
}

BigInteger operator-(const BigInteger &first, const BigInteger &second) {
  BigInteger big_integer = first;
  big_integer -= second;
  return big_integer;
}

// multiplication using Karasuba method
BigInteger operator*(const BigInteger &first, const BigInteger &second) {
  if (first.IsZero() || second.IsZero()) {
    return BigInteger(0);
  }
  if (first.Size() == 1 && second.Size() == 1) {
    long long res = static_cast<long long>(first.nums_[0]) *
                    static_cast<long long>(second.nums_[0]);  // cast
    if (first.positive_ != second.positive_) {
      res *= (-1);
    }
    return BigInteger(res);
  }
  size_t len = std::max(first.Size(), second.Size());
  size_t new_len = len / 2;

  BigInteger a = first.Abs();
  BigInteger b = second.Abs();

  a.nums_.resize(len, 0);
  b.nums_.resize(len, 0);

  BigInteger big_integer_a_1 = Shift(0, new_len, a);
  BigInteger big_integer_a_2 = Shift(new_len, a.Size(), a);
  BigInteger big_integer_b_1 = Shift(0, new_len, b);
  BigInteger big_integer_b_2 = Shift(new_len, b.Size(), b);

  BigInteger mul_1 = big_integer_a_1 * big_integer_b_1;
  BigInteger mul_2 = big_integer_a_2 * big_integer_b_2;

  BigInteger sum = mul_1;
  BigInteger mul_3 = ((big_integer_a_1 + big_integer_a_2) *
                      (big_integer_b_1 + big_integer_b_2))
                         .InsertZeros(new_len) -
                     mul_1.InsertZeros(new_len) - mul_2.InsertZeros(new_len);

  sum += mul_3;
  sum += mul_2.InsertZeros(new_len);

  sum.positive_ = (first.positive_ == second.positive_);
  return sum;
}

// division using binary search
BigInteger operator/(const BigInteger &first, const BigInteger &second) {
  if (first.IsZero()) {
    return BigInteger(0);
  }
  BigInteger result = BigInteger(0);  // zero
  BigInteger divider = second.Abs();
  BigInteger dividend = first.Abs();
  while (dividend >= divider) {
    BigInteger bot_bound = divider;
    size_t num_of_blocks = 0;

    if (dividend.Size() != divider.Size()) {
      num_of_blocks = dividend.Size() - divider.Size() - 1;
      bot_bound = InsertionOfZero(divider, num_of_blocks);
      if (InsertionOfZero(bot_bound, 1) <= dividend) {
        bot_bound.InsertZeros(1);
        ++num_of_blocks;
      }
    }

    uint64_t bottom_value = 1;
    uint64_t up_value = first.base;

    while (up_value - bottom_value != 1) {
      uint64_t mid_value = (bottom_value + up_value) / 2;
      if (BigInteger(static_cast<long long>(mid_value)) * bot_bound <=
          dividend) {
        bottom_value = mid_value;
      } else {
        up_value = mid_value;
      }
    }
    BigInteger substraction =
        BigInteger(static_cast<long long>(bottom_value)) * bot_bound;
    dividend -= substraction;
    result += (BigInteger(1).InsertZeros(num_of_blocks) *
               BigInteger(static_cast<long long>(bottom_value)));
  }
  result.positive_ = (first.positive_ == second.positive_);
  return result;
}

BigInteger operator%(const BigInteger &dividend, const BigInteger &divider) {
  return dividend - divider * (dividend / divider);
}

std::ostream &operator<<(std::ostream &output_stream,
                         const BigInteger &number) {
  output_stream << number.toString();

  return output_stream;
}

std::istream &operator>>(std::istream &stream, BigInteger &big_integer) {
  std::string input_string;
  stream >> input_string;
  big_integer = BigInteger(input_string);
  return stream;
}

bool operator>(const BigInteger &first, const BigInteger &second) {
  if (first.positive_ != second.positive_) {
    return first.positive_;
  }
  if (first.Size() != second.Size()) {
    if (first.positive_) {
      return first.Size() > second.Size();
    }
    return first.Size() < second.Size();
  }
  // if zeros
  if (first.Size() == 0) {
    return false;
  }

  int cur_size = static_cast<int>(first.Size() - 1);
  for (int i = cur_size; i >= 0; --i) {
    if (first.nums_[static_cast<size_t>(i)] !=
        second.nums_[static_cast<size_t>(i)]) {
      return first.positive_ == (first.nums_[static_cast<size_t>(i)] >
                                 second.nums_[static_cast<size_t>(i)]);
    }
  }
  return false;  // if ==
}

bool operator<(const BigInteger &first, const BigInteger &second) {
  return (second > first);
}

bool operator<=(const BigInteger &first, const BigInteger &second) {
  return !(first > second);
}

bool operator>=(const BigInteger &first, const BigInteger &second) {
  return !(first < second);
}

bool operator==(const BigInteger &first, const BigInteger &second) {
  return (!(first < second) && !(second < first));
}

bool operator!=(const BigInteger &first, const BigInteger &second) {
  return !(first == second);
}
