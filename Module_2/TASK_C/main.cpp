#include <cstring>
#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

class SuffixArray {
 public:
  explicit SuffixArray(const string &string, size_t alphabet_size,
                       const size_t &pos_sep)
      : length(string.size()),
        alphabet_size(alphabet_size),
        string_(string),
        pos_separator(pos_sep),
        sorted_suffix_(length) {
    BuildSuffixArray();
  }

  const size_t length;
  const size_t alphabet_size;
  const string &string_;
  const size_t pos_separator;
  vector<size_t> sorted_suffix_;

 private:
  void BuildSuffixArray() {
    vector<size_t> classes_equal(length);  // array of classes equal
    vector<size_t> occ(alphabet_size);
    size_t num_classes = 1;
    FirstPreparation(classes_equal, occ, num_classes);

    size_t cur_depth = 2;

    vector<size_t> new_sorted_suffix(length);
    vector<size_t> new_classes_equal(length);

    while (cur_depth < 2 * length) {
      size_t prev_depth = cur_depth / 2;

      // make sorted by 2nd value
      vector<size_t> cnt_occ =
          PocketSort(new_sorted_suffix, prev_depth, num_classes, classes_equal);

      UpdateSortedSuffix(cnt_occ, classes_equal, new_sorted_suffix);

      num_classes = UpdateClassesEqual(prev_depth, classes_equal);
      cur_depth *= 2;
    }
  }

  vector<size_t> PocketSort(vector<size_t> &new_sorted_suffix,
                            const size_t &prev_depth, const size_t &num_classes,
                            vector<size_t> &classes_equal) {
    for (size_t i = 0; i < length; ++i) {
      new_sorted_suffix[i] =
          ((length + sorted_suffix_[i]) - prev_depth) % length;
    }
    vector<size_t> cnt_occ(num_classes);  //  pocket sort

    for (auto i : classes_equal) {
      ++cnt_occ[i];
    }

    for (size_t i = 1; i < num_classes; ++i) {
      cnt_occ[i] += cnt_occ[i - 1];
    }
    return cnt_occ;
  }
  std::pair<size_t, size_t> DoubleClass(const size_t &i,
                                        const size_t &prev_depth,
                                        const vector<size_t> &classes_equal) {
    return {classes_equal[sorted_suffix_[i]],
            classes_equal[(sorted_suffix_[i] + prev_depth) % length]};
  }

  size_t UpdateClassesEqual(const size_t &prev_depth,
                            vector<size_t> &classes_equal) {
    size_t new_num_classes = 1;
    vector<size_t> new_classes_equal(length);
    for (size_t i = 1; i < length; ++i) {
      if (DoubleClass(i, prev_depth, classes_equal) !=
          DoubleClass(i - 1, prev_depth, classes_equal)) {
        ++new_num_classes;
      }
      new_classes_equal[sorted_suffix_[i]] = new_num_classes - 1;
    }
    for (size_t i = 0; i < length; ++i) {
      classes_equal[i] = new_classes_equal[i];
    }
    return new_num_classes;
  }
  void UpdateSortedSuffix(vector<size_t> cnt_occ, vector<size_t> &classes_equal,
                          vector<size_t> &new_sorted_suffix) {
    size_t num_step = length;
    while (num_step != 0) {
      --num_step;
      sorted_suffix_[cnt_occ[classes_equal[new_sorted_suffix[num_step]]] - 1] =
          new_sorted_suffix[num_step];
      --cnt_occ[classes_equal[new_sorted_suffix[num_step]]];
    }
  }

  void FirstPreparation(vector<size_t> &classes_equal, vector<size_t> &occ,
                        size_t &num_classes) {
    // init occurences
    for (size_t i = 0; i < length; ++i) {
      ++occ[size_t(string_[i])];
    }

    for (size_t i = 1; i < alphabet_size; ++i) {
      occ[i] += occ[i - 1];
    }

    for (size_t i = 0; i < length; ++i) {
      sorted_suffix_[occ[size_t(string_[i])] - 1] = i;
      --occ[size_t(string_[i])];
    }
    // init classes_equal
    classes_equal[0] = 0;

    for (size_t i = 1; i < length; ++i) {
      if (string_[sorted_suffix_[i - 1]] != string_[sorted_suffix_[i]]) {
        ++num_classes;
      }
      classes_equal[sorted_suffix_[i]] = num_classes - 1;
    }
  }
};

size_t MinLength(const size_t &cur_length, const SuffixArray &suffix_array,
                 const size_t &next) {
  return std::min(((cur_length > suffix_array.pos_separator)
                       ? (suffix_array.length - cur_length)
                       : (suffix_array.pos_separator - cur_length)),
                  ((next > suffix_array.pos_separator)
                       ? (suffix_array.length - next)
                       : (suffix_array.pos_separator - cur_length)));
}

void StepBuildLcpArray(vector<size_t> &reverse_suf, size_t cur_length,
                       vector<ssize_t> &lcp_array, size_t &cur_equalities,
                       const SuffixArray &suffix_array) {
  if (reverse_suf[cur_length] == suffix_array.length - 1) {
    lcp_array[suffix_array.length - 1] = -1;
    cur_equalities = 0;
    return;
  }

  if (cur_equalities > 0) {
    --cur_equalities;
  }
  size_t next = suffix_array.sorted_suffix_[reverse_suf[cur_length] + 1];
  size_t min_length = MinLength(cur_length, suffix_array, next);
  while (cur_equalities < min_length &&
         suffix_array.string_[cur_equalities + cur_length] ==
             suffix_array.string_[cur_equalities + next]) {
    ++cur_equalities;
  }
  lcp_array[reverse_suf[cur_length]] = static_cast<ssize_t>(cur_equalities);
}

vector<ssize_t> BuildLcpArray(const SuffixArray &suffix_array) {
  vector<ssize_t> lcp_array(suffix_array.length);
  vector<size_t> reverse_suf(suffix_array.length);
  for (size_t i = 0; i < suffix_array.length; ++i) {
    reverse_suf[suffix_array.sorted_suffix_[i]] = i;
  }
  size_t cur_equalities = 0;

  for (size_t cur_length = 0; cur_length < suffix_array.length; ++cur_length) {
    StepBuildLcpArray(reverse_suf, cur_length, lcp_array, cur_equalities,
                      suffix_array);
  }
  return lcp_array;
}

bool SuffixSide(const size_t &first_pos, const size_t &separator_pos) {
  return first_pos < separator_pos;
}
bool FindString(const SuffixArray &suffix_array,
                const vector<ssize_t> &lcp_array, long long k,
                size_t &idx_of_begin_str, size_t &idx_of_end_str) {
  unsigned long long num_equal_str = 0;
  size_t min_lcp = 0;
  bool is_exists = false;
  for (size_t i = 2; i < suffix_array.length - 1; ++i) {
    if (SuffixSide(suffix_array.sorted_suffix_[i],
                   suffix_array.pos_separator) !=
        SuffixSide(suffix_array.sorted_suffix_[i + 1],
                   suffix_array.pos_separator)) {
      num_equal_str +=
          lcp_array[i] -
          std::min(min_lcp,
                   static_cast<size_t>(
                       lcp_array[i]));  // sub LCP(i, prev_pair # and $)
      if (num_equal_str >= k) {
        idx_of_begin_str = suffix_array.sorted_suffix_[i];
        idx_of_end_str =
            suffix_array.sorted_suffix_[i] + lcp_array[i] - (num_equal_str - k);
        is_exists = true;
        break;
      }
      min_lcp = lcp_array[i];
      continue;
    }
    min_lcp = std::min(min_lcp, static_cast<size_t>(lcp_array[i]));
  }
  return is_exists;
}

void OutputAnswer(long long k, const SuffixArray &suffix_array,
                  const vector<ssize_t> &lcp_array) {
  size_t idx_of_begin_str = 0;
  size_t idx_of_end_str = 0;
  if (FindString(suffix_array, lcp_array, k, idx_of_begin_str,
                 idx_of_end_str)) {
    for (size_t i = idx_of_begin_str; i < idx_of_end_str; ++i) {
      cout << suffix_array.string_[i];
    }
  } else {
    cout << -1;
  }
}

int main() {
  const size_t alphabet_size = 256;
  string s1;
  string s2;
  cin >> s1 >> s2;
  const size_t pos_separator = s1.size();
  string s = s1 + char(0) + s2 + char(1);  // char(0) means '$', char(1) - '#'
  SuffixArray suffix_array = SuffixArray(s, alphabet_size, pos_separator);
  long long k;
  cin >> k;
  vector<ssize_t> lcp_array = BuildLcpArray(suffix_array);
  OutputAnswer(k, suffix_array, lcp_array);
}
