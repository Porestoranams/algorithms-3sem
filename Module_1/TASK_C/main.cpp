#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

using ::std::cin;
using ::std::cout;
using ::std::string;
using ::std::vector;

struct AhoCorasickNode {
  std::vector<size_t> terminal_ids_;
  std::map<char, AhoCorasickNode> trie_transitions_;
  std::unordered_map<char, AhoCorasickNode *> automaton_transitions_cache_;
  AhoCorasickNode *suffix_link_ = nullptr;
  AhoCorasickNode *terminal_link_ = nullptr;
  bool HasAutomatonCacheTransition(char ch);
  bool HasTrieTransition(char ch);
};
bool AhoCorasickNode::HasTrieTransition(char ch) {
  return (trie_transitions_.find(ch) != trie_transitions_.end());
}
bool AhoCorasickNode::HasAutomatonCacheTransition(char ch) {
  return (automaton_transitions_cache_.find(ch) !=
          automaton_transitions_cache_.end());
}

AhoCorasickNode *GetAutomatonTransition(AhoCorasickNode *node, char ch) {
  if (node->HasAutomatonCacheTransition(ch)) {
    return node->automaton_transitions_cache_[ch];
  }
  if (node->suffix_link_ != nullptr) {
    return node->automaton_transitions_cache_[ch] =
               GetAutomatonTransition(node->suffix_link_, ch);
  }
  return node;
}

class NodeReference {
 public:
  NodeReference() = default;
  explicit NodeReference(AhoCorasickNode *node) : node_(node) {}

  NodeReference Next(char ch) const {
    return NodeReference(GetAutomatonTransition(node_, ch));
  }

  template <class Callback>
  void ForEachMatch(Callback cb) const {
    auto cur_node = node_;
    while (cur_node != nullptr) {
      for (auto term_id : cur_node->terminal_ids_) {
        cb(term_id);
      }
      cur_node = cur_node->terminal_link_;
    }
  }

 private:
  AhoCorasickNode *node_ = nullptr;
};

class AhoCorasick {
 public:
  AhoCorasick() = default;
  AhoCorasick(const AhoCorasick &) = delete;
  AhoCorasick &operator=(const AhoCorasick &) = delete;
  AhoCorasick(AhoCorasick &&) = delete;
  AhoCorasick &operator=(AhoCorasick &&) = delete;
  NodeReference Root() { return NodeReference(&root_); }

 private:
  friend class AhoCorasickBuilder;
  AhoCorasickNode root_;
};

class AhoCorasickBuilder {
 public:
  void AddString(std::string temp_string, size_t id) {
    strings_.push_back(std::move(temp_string));
    ids_.push_back(id);
  }

  std::unique_ptr<AhoCorasick> Build() {
    auto automaton = std::make_unique<AhoCorasick>();
    automaton->root_ = AhoCorasickNode();
    for (size_t i = 0; i < strings_.size(); ++i) {
      AddString(&automaton->root_, strings_[i], ids_[i]);
    }
    CalculateLinks(&automaton->root_);
    return automaton;
  }

 private:
  static void AddString(AhoCorasickNode *root, const std::string &string,
                        size_t id) {
    AhoCorasickNode *temp_node = root;
    for (char i : string) {
      if (!temp_node->HasTrieTransition(i)) {
        temp_node->trie_transitions_[i] = AhoCorasickNode();
      }
      temp_node->automaton_transitions_cache_[i] =
          &temp_node->trie_transitions_[i];
      temp_node = &temp_node->trie_transitions_[i];
    }
    temp_node->terminal_ids_.push_back(id);
  }

  static void CalculateLinks(AhoCorasickNode *root) {
    root->suffix_link_ = nullptr;
    root->terminal_link_ = nullptr;
    std::queue<AhoCorasickNode *> q;
    q.push(root);
    while (!q.empty()) {
      auto cur_node = q.front();
      q.pop();
      for (auto &child_node : cur_node->trie_transitions_) {
        auto cur_char = child_node.first;
        auto cur_child_node = &child_node.second;
        q.push(cur_child_node);
        if (cur_node->suffix_link_ == nullptr) {
          cur_child_node->suffix_link_ = root;
          cur_child_node->terminal_link_ = nullptr;
          continue;
        }
        cur_child_node->suffix_link_ =
            GetAutomatonTransition(cur_node->suffix_link_, cur_char);
        if (cur_child_node->suffix_link_->terminal_ids_.empty()) {
          child_node.second.terminal_link_ =
              cur_child_node->suffix_link_->terminal_link_;
        } else {
          cur_child_node->terminal_link_ = cur_child_node->suffix_link_;
        }
      }
    }
  }
  std::vector<std::string> strings_;
  std::vector<size_t> ids_;
};

vector<size_t> Split(const string &string, char delimiter,
                     vector<std::string> &patterns) {
  vector<size_t> wild_card_pos;
  std::string cur_string;
  for (size_t i = 0; i < string.size(); ++i) {
    if (string[i] == delimiter && !cur_string.empty()) {
      patterns.push_back(cur_string);
      wild_card_pos.push_back(i);
      cur_string.clear();
    } else if (string[i] != delimiter) {
      cur_string += string[i];
    }
  }
  if (!cur_string.empty()) {
    patterns.push_back(cur_string);
    wild_card_pos.push_back(string.size());
  }
  return wild_card_pos;
}

class WildcardMatcher {
 public:
  WildcardMatcher(const std::string &pattern, char wildcard)
      : occurrences_by_offset_(pattern.size()) {
    AhoCorasickBuilder builder;
    vector<size_t> wildcard_positions;
    vector<string> patterns;
    wildcard_positions = Split(pattern, wildcard, patterns);
    for (size_t i = 0; i < patterns.size(); ++i) {
      builder.AddString(patterns[i], wildcard_positions[i]);
    }
    automaton_ = builder.Build();
    state_ = automaton_->Root();
    number_of_words_ = wildcard_positions.size();
    pattern_length_ = pattern.size();
  }

  template <class Callback>
  void Scan(char character, Callback on_match) {
    state_ = state_.Next(character);
    state_.ForEachMatch(
        [&](size_t i) { ++occurrences_by_offset_[pattern_length_ - i]; });
    if (occurrences_by_offset_[0] == number_of_words_) {
      on_match();
    }
    UpdateDeque();
  }

 private:
  void UpdateDeque() {
    occurrences_by_offset_.pop_front();
    occurrences_by_offset_.push_back(0);
  }
  std::deque<size_t> occurrences_by_offset_;
  size_t number_of_words_;
  size_t pattern_length_;
  std::unique_ptr<AhoCorasick> automaton_;
  NodeReference state_;
};

int main() {
  string pattern;
  cin >> pattern;
  size_t pattern_size = pattern.size();
  WildcardMatcher wildcard_matcher = WildcardMatcher(pattern, '?');
  char text_i;
  size_t cur_pos = 0;
  string text;
  cin >> text;
  for (char i : text) {
    wildcard_matcher.Scan(
        i, [&]() { (cout << cur_pos - pattern_size + 1 << ' '); });
    ++cur_pos;
  }
}