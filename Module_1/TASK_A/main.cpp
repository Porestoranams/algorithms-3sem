#include <iostream>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

class KMPSearcher {
 public:
  explicit KMPSearcher(const string &pattern)
      : pattern_(pattern), pattern_prefix_function_(pattern_.size()) {
    for (size_t i = 1; i < pattern_.size(); ++i) {
      size_t possible_prefix_value = pattern_prefix_function_[i - 1];
      while (possible_prefix_value != 0 &&
             pattern_[i] != pattern_[possible_prefix_value]) {
        possible_prefix_value =
            pattern_prefix_function_[possible_prefix_value - 1];
      }
      if (pattern_[i] == pattern_[possible_prefix_value]) {
        pattern_prefix_function_[i] = possible_prefix_value + 1;
      }
    }
  }

  template <class Callback>
  void Search(const string &text, Callback on_occurence) const {
    size_t prev = 0;
    for (size_t i = 0; i < text.size(); ++i) {
      size_t possible_prefix_value = prev;
      while (possible_prefix_value != 0 &&
             ((possible_prefix_value == pattern_.size())
                  ? (true)
                  : (text[i] != pattern_[possible_prefix_value]))) {
        possible_prefix_value =
            pattern_prefix_function_[possible_prefix_value - 1];
      }
      if (pattern_[possible_prefix_value] == text[i]) {
        possible_prefix_value += 1;
      }
      if (possible_prefix_value == pattern_.size()) {
        on_occurence(i - pattern_.size() + 1);
      }
      prev = possible_prefix_value;
    }
  }

 private:
  const string &pattern_;
  std::vector<size_t> pattern_prefix_function_;
};
int main() {
  string s;
  string text;
  cin >> s >> text;
  KMPSearcher k = KMPSearcher(s);
  k.Search(text, [](size_t i) { cout << i << ' '; });
}
