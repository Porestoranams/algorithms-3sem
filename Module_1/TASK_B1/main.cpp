#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

class StringBuilder {
 public:
  explicit StringBuilder(vector<size_t> &prefix_function)
      : prefix_function_(prefix_function) {}

  template <class Callback>
  void Construct(Callback callback) {
    vector<size_t> ans_vector(prefix_function_.size());
    ans_vector[0] = 0;
    for (size_t i = 1; i < prefix_function_.size(); ++i) {
      if (prefix_function_[i] != 0) {
        ans_vector[i] = ans_vector[prefix_function_[i] - 1];
      } else {
        vector<size_t> proh_letters(alphabet_size);
        size_t j = prefix_function_[i - 1];
        while (j != 0) {
          proh_letters[ans_vector[j]] = 1;
          j = prefix_function_[j - 1];
        }
        proh_letters[0] = 1;
        for (size_t k = 0; k < alphabet_size; ++k) {
          if (proh_letters[k] == 0) {
            ans_vector[i] = k;
            break;
          }
        }
      }
    }
    for (const size_t &i : ans_vector) {
      callback(i);
    }
  }

 private:
  const vector<size_t> &prefix_function_;
  const size_t alphabet_size = 'z' - 'a' + 1;
};

int main() {
  vector<size_t> prefix_function;
  const int the_least_symbol_alph = 'a';
  size_t a;
  while (cin >> a) {
    prefix_function.push_back(a);
  }
  auto s = StringBuilder(prefix_function);
  s.Construct([](size_t i) { cout << char(i + the_least_symbol_alph); });
}
