#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::string;
using std::vector;

class StringBuilder {
 public:
  explicit StringBuilder(vector<size_t> &z_function, const size_t &alph_size)
      : alphabet_size(alph_size),
        z_function_(z_function),
        ans_string_(z_function_.size()) {}
  template <class Callback>
  void Construct(Callback callback) {
    ans_string_[0] = 0;
    vector<size_t> bad_let(alphabet_size);
    size_t cur_size = 1;
    for (size_t i = 1; i < z_function_.size(); ++i) {
      ConstructStep(i, cur_size, bad_let);
    }
    for (const size_t &i : ans_string_) {
      callback(i);
    }
  }
  const size_t alphabet_size = 'z' - 'a' + 1;

 private:
  void ConstructStep(size_t &cur_pos, size_t &cur_size,
                     vector<size_t> &forbidden_positions_for_letters) {
    if (cur_size == cur_pos) {
      if (z_function_[cur_pos] == 0) {
        forbidden_positions_for_letters[ans_string_[0]] = cur_size;

        for (size_t j = 0; j < alphabet_size; ++j) {
          if (forbidden_positions_for_letters[j] != cur_pos) {
            ans_string_[cur_size] = j;
            ++cur_size;
            break;
          }
        }
      } else {
        for (size_t j = cur_pos; j < cur_pos + z_function_[cur_pos]; ++j) {
          ans_string_[j] = ans_string_[j - cur_pos];
        }
        forbidden_positions_for_letters[ans_string_[z_function_[cur_pos]]] =
            cur_pos + z_function_[cur_pos];
        cur_size += z_function_[cur_pos];
      }
    } else {
      if (z_function_[cur_pos] + cur_pos == cur_size) {
        forbidden_positions_for_letters[ans_string_[z_function_[cur_pos]]] =
            cur_size;
      } else if (z_function_[cur_pos] + cur_pos > cur_size) {
        for (size_t j = cur_size; j < z_function_[cur_pos] + cur_pos; ++j) {
          ans_string_[j] = ans_string_[j - cur_pos];
        }

        cur_size += cur_pos + z_function_[cur_pos] - cur_size;
        forbidden_positions_for_letters[ans_string_[z_function_[cur_pos]]] =
            cur_size;
      }
    }
  }
  vector<size_t> &z_function_;
  vector<size_t> ans_string_;
};
int main() {
  vector<size_t> z_function;
  size_t a;
  while (cin >> a) {
    z_function.push_back(a);
  }
  const size_t alphabet_size = 'z' - 'a' + 1;
  const int the_least_sym_alp = 'a';
  auto s = StringBuilder(z_function, alphabet_size);
  s.Construct([](size_t i) { cout << char(i + the_least_sym_alp); });
}
